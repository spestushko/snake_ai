import curses

from curses import KEY_UP, KEY_LEFT, KEY_RIGHT, KEY_DOWN
from random import randint
from enum import Enum


class Movement(Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3


class SnakeGame:

    def __init__(self, height=20, width=20, snake_size=3):
        self.snake_size = snake_size
        self.board_dimensions = {
            'height': height,
            'width': width
        }

        self.done = False
        self.score = 0
        self.window = None
        self.snake = []
        self.food = []

    def start(self):
        self.generate_snake(self.snake_size)
        self.generate_food()
        self.generate_board()
        return self.get_stats()

    def generate_board(self):
        curses.initscr()

        begin_y = begin_x = 0
        win = curses.newwin(self.board_dimensions['height'], self.board_dimensions['width'], begin_y, begin_x)
        curses.curs_set(False)

        win.nodelay(True)
        win.timeout(200)
        self.window = win

        self.render()

    def generate_snake(self, snake_size):
        base_offset_mult = 0.65
        base_offset_y = 5
        base_offset_x = 5

        y_pos = randint(base_offset_y, self.board_dimensions['height'] - base_offset_y)
        x_pos = randint(base_offset_x, self.board_dimensions['width'] - base_offset_x)

        # 0 - horizontal
        # 1 - vertical
        horizontal = randint(0, 1) == 0

        for i in range(snake_size):
            point = [y_pos, x_pos + i] if horizontal else [y_pos + i, x_pos]
            self.snake.insert(0, point)

    def generate_food(self):
        food = []

        while not food:
            food = [randint(1, self.board_dimensions['height']), randint(1, self.board_dimensions['width'])]
            if food in self.snake:
                food = []

        self.food = food

    def render(self):
        self.window.clear()
        self.window.border(0)
        self.window.addstr(0, 2, f' Score : {str(self.score)} ')

        self.window.addch(self.food[0], self.food[1], 'x')

        for i, point in enumerate(self.snake):
            snake_body_element = 'x' if i == 0 else 'o'
            self.window.addch(point[0], point[1], snake_body_element)
        self.window.getch()

    def step(self, key):

        if self.done:
            self.end_game()

        self.add_point(key)

        if self.ate_food():
            self.score += 1
            self.generate_food()
        else:
            self.remove_point()

        self.check_collisions()
        self.render()

        return self.get_stats()

    def add_point(self, key):
        new_point_y = self.snake[0][0]
        new_point_x = self.snake[0][1]

        # Hmm...
        if key == Movement.UP.value:
            new_point_y -= 1
        elif key == Movement.RIGHT.value:
            new_point_x += 1
        elif key == Movement.DOWN.value:
            new_point_y += 1
        elif key == Movement.LEFT.value:
            new_point_x -= 1

        self.snake.insert(0, [new_point_y, new_point_x])

    def remove_point(self):
        self.snake.pop()

    def ate_food(self):
        return self.snake[0] == self.food

    def check_collisions(self):
        if (self.snake[0][0] == 0 or
                self.snake[0][0] == self.board_dimensions['width'] + 1 or
                self.snake[0][1] == 0 or
                self.snake[0][1] == self.board_dimensions['height'] + 1 or
                # Hmm...
                self.snake[0] in self.snake[1:-1]):
            self.done = True

    def get_stats(self):
        return self.done, self.score, self.snake, self.food

    def destroy_board(self):
        curses.endwin()

    def end_game(self):
        self.destroy_board()
        raise Exception("Game Over")


if __name__ == "__main__":
    game = SnakeGame()
    game.start()

    # Play the game
    for _ in range(20):
        game.step(randint(0, 3))
